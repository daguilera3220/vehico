new Chart(document.getElementById("myChart"), {
    type: 'line',
    responsive: true,
    data: {
      labels: ['', '20/10/2018', '21/10/2018', '22/10/2018', '23/10/2018', '24/10/2018', '25/10/2018', '', '', ''],
      datasets: [{
          data: [0,2,3,5,3,2,4],
          label: "Kilómetros",
          borderColor: "#3e95cd",
          fill: false
        }, {
            data: [2.667, 2.8099, 2.9508, 3.0927, 3.2376, 3.3795, 3.5184, 3.6673, 3.8102, 3.9531],
            label: 'Regresión Lineal',
            borderColor: '#ff0000',
            fill: false
        }
      ]
    },
    options: {
      title: {
        display: true,
        text: 'kilómetros/sesión'
      },
      responsive: true,
      maintainAspectRatio: false,
      scales: {
        yAxes: [{
            ticks: {
                beginAtZero:true
            }
        }]
      }
    }
  });
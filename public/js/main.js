/* eslint-env jquery, browser */
$(document).ready(() => {
  $('.multiple-items').slick({
  centerMode: true,
  centerPadding: '60px',
  slidesToShow: 3,
  arrows: true,
  focusOnSelect: true,
  swipeToSlide: true,
  responsive: [
      {
        breakpoint: 780,
        settings: {
          arrows: false,
          centerMode: true,
          centerPadding: '40px',
          slidesToShow: 1
        }
      }
    ]
  });
  $('.multiple-items').on('beforeChange', function(event, slick, currentSlide, nextSlide){
    console.log(nextSlide);
    if($('#vehicleType')){
      $('#vehicleType').val(nextSlide);
    }
  });

  var calendar = $('#calendar').fullCalendar({
     defaultView: 'agendaWeek',
     editable: false,
     selectable: true,
     allDaySlot: false,
     //header and other values
     select: function(start, end, allDay) {
         endtime = $.fullCalendar.formatDate(end,'h:mm tt');
         starttime = $.fullCalendar.formatDate(start,'ddd, MMM d, h:mm tt');
         var mywhen = starttime + ' - ' + endtime;
         $('#createEventModal #apptStartTime').val(start);
         $('#createEventModal #apptEndTime').val(end);
         $('#createEventModal #apptAllDay').val(allDay);
         $('#createEventModal #when').text(mywhen);
         $('#createEventModal').modal('show');
      }
   });

   $('#submitButton').on('click', function(e){
    // We don't want this to act as a link so cancel the link action
    e.preventDefault();

    doSubmit();
  });

  function doSubmit(){
    $("#createEventModal").modal('hide');
    $("#calendar").fullCalendar('renderEvent',
        {
            title: $('#mymail').val(),
            start: new Date($('#apptStartTime').val()),
            end: new Date($('#apptEndTime').val()),
        },
        true);
   }

  getEvents = () =>{
    var id = $('#idVehicle').val();
    var calEvents = $('#calendar').fullCalendar('clientEvents');
    $('#eventArray').val(calEvents);
    $('#saveSchedule').submit();
  }
});

function agregarEjercicio(){
  var suh = $('.agregarrutina input').val();
  $(".atletainicio .lists").append("<div class='col-12 col-sm-6'><div class='input-group mb-3'><div class='input-group-prepend'><span class='input-group-text' id='inputGroup-sizing-default'>"+suh+"</span></div><input class='form-control' type='text' /></div></div>");
}

function showToast(){
  var x = document.getElementById("snackbar");
    x.className = "show";
    setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
}

addMoreOwners = () =>{
  if($('#dueno2').hasClass('d-none')){
    $('#dueno2').removeClass('d-none');
  }else if($('#dueno3').hasClass('d-none')){
    $('#dueno3').removeClass('d-none');
  }
}

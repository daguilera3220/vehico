var selected = 0;

function selectUser(userType) {

    if(userType == 1) {

        $("#user-fan").css("box-shadow", "0 0 5px rgba(81, 203, 238, 1)");
        $("#user-fan").css("border", "1px solid rgba(81, 203, 238, 1)");
        selected = 1;
        $("#user-deportista").css("border", "");
        $("#user-deportista").css("box-shadow", "0 0.46875rem 2.1875rem rgba(90,97,105,.1), 0 0.9375rem 1.40625rem rgba(90,97,105,.1), 0 0.25rem 0.53125rem rgba(90,97,105,.12), 0 0.125rem 0.1875rem rgba(90,97,105,.1)");
        $("#user-coach").css("border", "");
        $("#user-coach").css("box-shadow", "0 0.46875rem 2.1875rem rgba(90,97,105,.1), 0 0.9375rem 1.40625rem rgba(90,97,105,.1), 0 0.25rem 0.53125rem rgba(90,97,105,.12), 0 0.125rem 0.1875rem rgba(90,97,105,.1)");

        var state = {}, newUrl = "1";
        window.history.pushState(state, "Page Title", newUrl);
        var productId = 1;
        window.history.pushState({productId: productId} , "", "/signup/" + productId);
    } else if (userType == 2) {

        $("#user-deportista").css("box-shadow", "0 0 5px rgba(81, 203, 238, 1)");
        $("#user-deportista").css("border", "1px solid rgba(81, 203, 238, 1)");
        selected = 2;
        $("#user-fan").css("border", "");
        $("#user-fan").css("box-shadow", "0 0.46875rem 2.1875rem rgba(90,97,105,.1), 0 0.9375rem 1.40625rem rgba(90,97,105,.1), 0 0.25rem 0.53125rem rgba(90,97,105,.12), 0 0.125rem 0.1875rem rgba(90,97,105,.1)");
        $("#user-coach").css("border", "");
        $("#user-coach").css("box-shadow", "0 0.46875rem 2.1875rem rgba(90,97,105,.1), 0 0.9375rem 1.40625rem rgba(90,97,105,.1), 0 0.25rem 0.53125rem rgba(90,97,105,.12), 0 0.125rem 0.1875rem rgba(90,97,105,.1)");
        
        var state = {}, newUrl = "2";
        window.history.pushState(state, "Page Title", newUrl);
        var productId = 2;
        window.history.pushState({productId: productId} , "", "/signup/" + productId);
    } else if(userType == 3) {
        
        $("#user-coach").css("box-shadow", "0 0 5px rgba(81, 203, 238, 1)");
        $("#user-coach").css("border", "1px solid rgba(81, 203, 238, 1)");
        selected = 3;
        $("#user-fan").css("border", "");
        $("#user-fan").css("box-shadow", "0 0.46875rem 2.1875rem rgba(90,97,105,.1), 0 0.9375rem 1.40625rem rgba(90,97,105,.1), 0 0.25rem 0.53125rem rgba(90,97,105,.12), 0 0.125rem 0.1875rem rgba(90,97,105,.1)");
        $("#user-deportista").css("border", "");
        $("#user-deportista").css("box-shadow", "0 0.46875rem 2.1875rem rgba(90,97,105,.1), 0 0.9375rem 1.40625rem rgba(90,97,105,.1), 0 0.25rem 0.53125rem rgba(90,97,105,.12), 0 0.125rem 0.1875rem rgba(90,97,105,.1)");


        var state = {}, newUrl = "3";
        window.history.pushState(state, "Page Title", newUrl);
        var productId = 3;
        window.history.pushState({productId: productId} , "", "/signup/" + productId);
    }
}
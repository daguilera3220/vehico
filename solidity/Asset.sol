pragma solidity >=0.4.22 <0.6.0;

contract Asset {
    address public owner;
    address public creator;
    string public name="red car";
    
    mapping(address => uint8) public balanceOf;
    
    event Transfer(
        address indexed _from,
        address indexed _to,
        uint8 _value
    );
    
    constructor(address _owner) public {
        balanceOf[_owner]=100;
        creator = msg.sender;
    }

    function transfer(address _to, uint8 _value) public returns (bool success) {
        require(balanceOf[msg.sender] >= _value);

        balanceOf[msg.sender] -= _value;
        balanceOf[_to] += _value;

        emit Transfer(msg.sender, _to, _value);

        return true;
    }
    
    function transferFrom(address _from, address _to, uint8 _value) public returns (bool success) {
        require(_value <= balanceOf[_from]);

        balanceOf[_from] -= _value;
        balanceOf[_to] += _value;
        
        emit Transfer(_from, _to, _value);

        return true;
    }

}
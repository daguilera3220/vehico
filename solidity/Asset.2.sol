pragma solidity >=0.4.22 <0.6.0;

contract Asset {
    address public creator;
    string public name="red car de las 00:52";
    
    mapping(address => uint256) public balanceOf;
    
    event Transfer(
        address indexed _from,
        address indexed _to,
        uint256 _value
    );
    
    constructor(address _owner) public {
        creator = msg.sender;
        balanceOf[_owner]=80;
        balanceOf[creator]=20;
    }

    function viewBalance(address _who) public view returns (uint256 _value) {
        return balanceOf[_who];
    }

    function transfer(address _to, uint256 _value) public returns (bool success) {
        require(balanceOf[msg.sender] >= _value);

        balanceOf[msg.sender] -= _value;
        balanceOf[_to] += _value;

        emit Transfer(msg.sender, _to, _value);

        return true;
    }
    
    function transferFrom(address _from, address _to, uint8 _value) public returns (bool success) {
        require(_value <= balanceOf[_from]);

        balanceOf[_from] -= _value;
        balanceOf[_to] += _value;
        
        emit Transfer(_from, _to, _value);

        return true;
    }

}
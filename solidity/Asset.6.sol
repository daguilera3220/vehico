pragma solidity >=0.4.22 <0.6.0;

contract Asset {
    address public creator;
    string public name;
    bool public initialized=false;
    address[] public owners;
    
    mapping(address => uint256) public balanceOf;
    
    event Transfer(
        address indexed _from,
        address indexed _to,
        uint256 _qty
    );
    
    function init(address[] memory _owners, uint256[] memory _values, string memory _name) public {
        if (!initialized) {
            creator = msg.sender;
            name=_name;
            uint256 acum=0;
            for (uint i = 0; i < _owners.length; i++) {
                balanceOf[_owners[i]]=_values[i];
                acum += _values[i];
            }
            if (acum!=100){
                revert();
            }
            owners=_owners;
            initialized=true;
        }else{
            revert();
        }
    }

    function viewBalance(address _who) public view returns (uint256 balance) {
        return balanceOf[_who];
    }

    function viewOwners() public view returns (address[] memory res) {
        return owners;
    }

    function transfer(address _to, uint256 _qty) public returns (bool success) {
        if (initialized){
            require(balanceOf[msg.sender] >= _qty);
    
            balanceOf[msg.sender] -= _qty;
            balanceOf[_to] += _qty;
    
            emit Transfer(msg.sender, _to, _qty);
        }else{
            revert();
        }

        return true;
    }
    
    function transferFrom(address _from, address _to, uint8 _qty) public returns (bool success) {
        if (initialized){
            if (msg.sender != creator){
                revert();
            }
            
            require(_qty <= balanceOf[_from]);
    
            balanceOf[_from] -= _qty;
            balanceOf[_to] += _qty;
            
            emit Transfer(_from, _to, _qty);
        }else{
            revert();
        }
        return true;
    }

}
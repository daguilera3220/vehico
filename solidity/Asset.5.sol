pragma solidity >=0.4.22 <0.6.0;

contract Asset {
    address public creator;
    string public name;
    
    mapping(address => uint256) public balanceOf;
    
    event Transfer(
        address indexed _from,
        address indexed _to,
        uint256 _qty
    );
    
    constructor(address[] memory _owners,uint256[] memory _values,string memory _name) public {
        creator = msg.sender;
        name=_name;
        uint256 acum=0;
        for (uint i = 0; i < _owners.length; i++) {
            balanceOf[_owners[i]]=_values[i];
            acum += _values[i];
        }
        if (acum!=100){
            revert();
        }
    }

    function viewBalance(address _who) public view returns (uint256 _qty) {
        return balanceOf[_who];
    }


    function transfer(address _to, uint256 _qty) public returns (bool success) {
        require(balanceOf[msg.sender] >= _qty);

        balanceOf[msg.sender] -= _qty;
        balanceOf[_to] += _qty;

        emit Transfer(msg.sender, _to, _qty);

        return true;
    }
    
    function transferFrom(address _from, address _to, uint8 _qty) public returns (bool success) {
        if (msg.sender != creator){
            revert();
        }
        require(_qty <= balanceOf[_from]);

        balanceOf[_from] -= _qty;
        balanceOf[_to] += _qty;
        
        emit Transfer(_from, _to, _qty);

        return true;
    }

}
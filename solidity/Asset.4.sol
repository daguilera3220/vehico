pragma solidity >=0.4.22 <0.6.0;

contract Asset {
    address public creator;
    string public name="red azul 2:08";
    
    mapping(address => uint256) public balanceOf;
    
    event Transfer(
        address indexed _from,
        address indexed _to,
        uint256 _qty
    );
    
    constructor(address _owner) public {
        creator = msg.sender;
        balanceOf[_owner]=80;
        balanceOf[creator]=20;
    }

    function viewBalance(address _who) public view returns (uint256 _qty) {
        return balanceOf[_who];
    }

    function paso1(address _to) public returns (bool success) {
        require(balanceOf[msg.sender] >= 1);

        balanceOf[msg.sender] -= 1;
        balanceOf[_to] += 1;

        emit Transfer(msg.sender, _to, 1);

        return true;
    }
    
        function transfer(address _to, uint256 _qty) public returns (bool success) {
        require(balanceOf[msg.sender] >= _qty);

        balanceOf[msg.sender] -= _qty;
        balanceOf[_to] += _qty;

        emit Transfer(msg.sender, _to, _qty);

        return true;
    }
    
    function transferFrom(address _from, address _to, uint8 _qty) public returns (bool success) {
        require(_qty <= balanceOf[_from]);

        balanceOf[_from] -= _qty;
        balanceOf[_to] += _qty;
        
        emit Transfer(_from, _to, _qty);

        return true;
    }

}
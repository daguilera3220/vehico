/**
 * GET /
 * Home page.
 */
exports.index = (req, res) => {
  res.render('home', {
    title: 'Inicio'
  });
};

exports.fan = (req, res) => {
  res.render('fan/start', {
    title: 'Inicio'
  });
};

exports.community = (req, res) => {
  res.render('home/community', {
    title: 'Comunidad'
  });
};

exports.market = (req, res) => {
  res.render('home/market', {
    title: 'Mercado'
  });
};

exports.plantar = (req, res) => {
  res.render('home/plantar', {
    title: 'plantar'
  });
};

const Web3 = require('web3')
const rpcURL = "https://kovan.infura.io/d4da41f2ca1b4a2cbb1b82203300181c" //https://kovan.infura.io/d4da41f2ca1b4a2cbb1b82203300181c
const web3 = new Web3(rpcURL)

const abi = [{"constant":true,"inputs":[],"name":"creator","outputs":[{"name":"","type":"address"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"name","outputs":[{"name":"","type":"string"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"","type":"address"}],"name":"balanceOf","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"_to","type":"address"},{"name":"_qty","type":"uint256"}],"name":"transfer","outputs":[{"name":"success","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[{"name":"_who","type":"address"}],"name":"viewBalance","outputs":[{"name":"_value","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"_from","type":"address"},{"name":"_to","type":"address"},{"name":"_qty","type":"uint8"}],"name":"transferFrom","outputs":[{"name":"success","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"inputs":[{"name":"_owner","type":"address"}],"payable":false,"stateMutability":"nonpayable","type":"constructor"},{"anonymous":false,"inputs":[{"indexed":true,"name":"_from","type":"address"},{"indexed":true,"name":"_to","type":"address"},{"indexed":false,"name":"_qty","type":"uint256"}],"name":"Transfer","type":"event"}];
const account1 = '0x33e13827b70371Ecb034a8585888eEBa147f6Dd4' // S
const account2 = '0xAb0e462B78f2469B4d6721672d5E1607d563E41A' // LA

const privateKey1 = Buffer.from('YOUR_PRIVATE_KEY_1', 'hex')
const privateKey2 = Buffer.from('3C7DDE6477D67B5672F29A3249D3C94D1FC5D3E06CC178A72E6097DABAED3FA0', 'hex')

// Read the deployed contract - get the addresss from Etherscan
const contractAddress = '0x6BCdC6a60D75C8613e99D2FA654C816288842f63'
const contractABI = 0;

var contract = new web3.eth.Contract(abi, contractAddress)
// console.log(contract.name.call(err, result ≤))

contract.methods.balanceOf(account1).call((err, balance) => {
    console.log(balance)
})
  


// web3.eth.getBalance(address, (err, wei) => {
//     var balance = web3.utils.fromWei(wei, 'ether')
//     console.log(balance)
//   })
  
const express = require('express');
const app = express();
const Asset = require('../models/Asset');
const User = require('../models/User');
var router = express.Router();
const fetch = require("node-fetch");

exports.getNew = (req, res) => {
  res.render('vehicle/new', {
    title: 'Register New Vehicle'
  });
};

exports.getDetails = (req, res) => {
  Asset.findById(req.params.id).exec(function(err, asset){
    var ownerguy = {};
    asset.owners.forEach((ownerg)=>{
      if(req.user.id === ownerg.owner.id){
        ownerguy = ownerg;
      }
    });
    postData4("https://hackathon.dltstax.net/projects/demo/contexts/ETH_contract/contracts/"+asset.addressETH+"/methods/viewOwners", {})
      .then(data => {
        var resultOwners = []
        data.output[0].forEach((owner)=>{
          postData4("https://hackathon.dltstax.net/projects/demo/contexts/ETH_contract/contracts/"+asset.addressETH+"/methods/viewBalance",
          {"_who": owner}).then(data =>{
            ownerData = {
              owner: owner,
              qty: data.output
            }
            resultOwners.push(ownerData);
          });
        });
        setTimeout(()=>{
          User.find({'addressETH': req.user.addressETH}, function(err, usuario){
            res.render('vehicle/details', {
              asset: asset,
              me: req.user,
              ownerguy: ownerguy,
              buying: req.params.buying,
              resultOwners: resultOwners
            });
          });
        }, 2500);
      })
      .catch(error => console.error(error));

    function postData4(url = ``, data = {}) {
      // Default options are marked with *
      return fetch(url, {
          method: "POST", // *GET, POST, PUT, DELETE, etc.
          mode: "cors", // no-cors, cors, *same-origin
          // cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
          // credentials: "same-origin", // include, *same-origin, omit
          headers: {
              "Content-Type": "application/json",
              "Authorization": "0x5ef3e221f288b53e6f758d9171854359e9ee3b23f29fa3841afe90b8549dffe1",
              "ABI": asset.abi,
              // "Content-Type": "application/x-www-form-urlencoded",
          },
          redirect: "follow", // manual, *follow, error
          referrer: "no-referrer", // no-referrer, *client
          body: JSON.stringify(data), // body data type must match "Content-Type" header
      })
      .then(response => response.json()); // parses response to JSON
    }
  });
};

exports.addEvents = (req, res) =>{
  res.redirect('/home');
}

exports.sellPercentage = (req, res) =>{
  var ownerguy = req.user;
  Asset.findById(req.params.id, function(err, asset){
    var sendJson = {"_from": req.user.addressETH, "_to": req.body.to, "_qty": parseInt(req.body.percentage)}
    console.log(sendJson);
    console.log();
    postData5("https://hackathon.dltstax.net/projects/demo/contexts/ETH_contract/contracts/"+asset.addressETH+"/methods/transferFrom",
    sendJson).then(data =>{
      console.log(data);
    });
    function postData5(url = ``, data = {}) {
      // Default options are marked with *
      return fetch(url, {
          method: "POST", // *GET, POST, PUT, DELETE, etc.
          mode: "cors", // no-cors, cors, *same-origin
          // cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
          // credentials: "same-origin", // include, *same-origin, omit
          headers: {
              "Content-Type": "application/json",
              "Authorization": "0x5ef3e221f288b53e6f758d9171854359e9ee3b23f29fa3841afe90b8549dffe1",
              "ABI": asset.abi,
              // "Content-Type": "application/x-www-form-urlencoded",
          },
          redirect: "follow", // manual, *follow, error
          referrer: "no-referrer", // no-referrer, *client
          body: JSON.stringify(data), // body data type must match "Content-Type" header
      })
      .then(response => response.json()); // parses response to JSON
    }
    asset.onSale = true;
    asset.owners.forEach((owner)=>{
      if(owner.owner == ownerguy.owner._id){
        owner.selling = req.body.percentage;
      }
    });
    asset.save();
    req.flash('success', { msg: 'Your Transaction is arriving to the blockchain!' });
    res.redirect('/home');
  });
}

exports.postRegister = (req, res) => {
  vehiculo = new Asset(req.body);

  postData2('https://hackathon.dltstax.net/projects/demo/contexts/ETH_contract/contracts',
    "pragma solidity >=0.4.22 <0.6.0;contract Asset { address public creator; string public name; bool public initialized=false; address[] public owners; mapping(address => uint256) public balanceOf; event Transfer( address indexed _from, address indexed _to, uint256 _qty ); function init(address[] memory _owners, uint256[] memory _values, string memory _name) public { if (!initialized) { creator = msg.sender; name=_name; uint256 acum=0; for (uint i = 0; i < _owners.length; i++) { balanceOf[_owners[i]]=_values[i]; acum += _values[i]; } if (acum!=100){ revert(); } owners=_owners; initialized=true; }else{ revert(); } } function viewBalance(address _who) public view returns (uint256 balance) { return balanceOf[_who]; } function viewOwners() public view returns (address[] memory res) { return owners; } function transfer(address _to, uint256 _qty) public returns (bool success) { if (initialized){ require(balanceOf[msg.sender] >= _qty); balanceOf[msg.sender] -= _qty; balanceOf[_to] += _qty; emit Transfer(msg.sender, _to, _qty); }else{ revert(); } return true; } function transferFrom(address _from, address _to, uint8 _qty) public returns (bool success) { if (initialized){ require(_qty <= balanceOf[_from]); balanceOf[_from] -= _qty; balanceOf[_to] += _qty; emit Transfer(_from, _to, _qty); }else{ revert(); } return true; }}")
    .then(data => {
      console.log(JSON.stringify(data));
      vehiculo.abi = data.abi;
      vehiculo.addressETH = data.contractAddress;
      vehiculo.save();
      User.find({}, function (err, users) {
        res.render('vehicle/owners',{
          asset: vehiculo,
          users: users,
          contador: 1,
          me: req.user
        });
      });
    }) // JSON-string from `response.json()` call
    .catch(error => console.error(error));
  function postData2(url = ``, data = ``) {
    // Default options are marked with *
      return fetch(url, {
          method: "POST", // *GET, POST, PUT, DELETE, etc.
          mode: "cors", // no-cors, cors, *same-origin
          // cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
          // credentials: "same-origin", // include, *same-origin, omit
          headers: {
              "Content-Type": "application/solidity",
              "Authorization": "0xf426fa3e772a91ab41d51c1e91a70f4492d29d0102da4a5fa40be9378acb3700",
              "Originator-Ref": "0x92a56f63bba47e6bfabd49b802524b142b1e6978",
              // "Content-Type": "application/x-www-form-urlencoded",
          },
          redirect: "follow", // manual, *follow, error
          referrer: "no-referrer", // no-referrer, *client
          body: data, // body data type must match "Content-Type" header
      })
      .then(response => response.json()); // parses response to JSON
  }
}

exports.postOwners = (req, res) => {
  const id = req.params.id;
  var arrayOwners = [];
  var percentage = req.body.per0;
  User.findById(req.body.dueno0, function (err, user) {
    user.vehicles.push(id);
    user.save();
    var dueno = {
      owner: user._id,
      addressETH: user.addressETH,
      qty: req.body.per0
    }
    arrayOwners.push(dueno);
  });
  if(req.body.dueno1 != 0){
    percentage += req.body.per1;
    User.findById(req.body.dueno1, function (err, user) {
      user.vehicles.push(id);
      user.save();
      var dueno = {
        owner: user._id,
        addressETH: user.addressETH,
        qty: req.body.per1
      }
      arrayOwners.push(dueno);
    });
  }
  if(req.body.dueno2 != 0){
    percentage += req.body.per2;
    User.findById(req.body.dueno2, function (err, user) {
      user.vehicles.push(id);
      user.save();
      var dueno = {
        owner: user._id,
        addressETH: user.addressETH,
        qty: req.body.per2
      }
      arrayOwners.push(dueno);
    });
  }
  if(req.body.dueno3 != 0){
    percentage += req.body.per3;
    User.findById(req.body.dueno3, function (err, user) {
      user.vehicles.push(id);
      user.save();
      var dueno = {
        owner: user._id,
        addressETH: user.addressETH,
        qty: req.body.per3
      }
      arrayOwners.push(dueno);
    });
  }

  Asset.findById(id, function (err, asset) {
    var ownersBc = [];
    var valuesBc = [];
    var name = "";
    console.log(arrayOwners);
    arrayOwners.forEach((owner)=>{
      ownersBc.push(owner.addressETH);
      valuesBc.push(parseInt(owner.qty));
    });
    name = ""+asset.name+"|"+asset.brand+"|"+asset.year;
    var jsonSend = {
      "_owners": ownersBc,
      "_values": valuesBc,
      "_name": name
    }
    console.log(jsonSend);
    postData3("https://hackathon.dltstax.net/projects/demo/contexts/ETH_contract/contracts/"+asset.addressETH+"/methods/init", jsonSend)
      .then(data => {
        console.log(JSON.stringify(data));
        req.flash('success', { msg: 'Your New Vehicle Contract is arriving to the blockchain!' });
        res.redirect('/home');
      })
      .catch(error => console.error(error));

    function postData3(url = ``, data = {}) {
      // Default options are marked with *
        return fetch(url, {
            method: "POST", // *GET, POST, PUT, DELETE, etc.
            mode: "cors", // no-cors, cors, *same-origin
            // cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
            // credentials: "same-origin", // include, *same-origin, omit
            headers: {
                "Content-Type": "application/json",
                "Authorization": "0x5ef3e221f288b53e6f758d9171854359e9ee3b23f29fa3841afe90b8549dffe1",
                "ABI": asset.abi,
                // "Content-Type": "application/x-www-form-urlencoded",
            },
            redirect: "follow", // manual, *follow, error
            referrer: "no-referrer", // no-referrer, *client
            body: JSON.stringify(data), // body data type must match "Content-Type" header
        })
        .then(response => response.json()); // parses response to JSON
    }
  });
}

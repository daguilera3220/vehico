/**
 * GET /
 * Home page.
 */
exports.index = (req, res) => {
  res.render('index', {
    title: 'Inicio'
  });
};

exports.atleta = (req, res) => {
  res.render('atleta/index', {
    title: 'Atleta'
  });
};

exports.coach = (req, res) => {
  res.render('coach/index', {
    title: 'Coach'
  });
};

exports.home = (req, res) => {
  res.render('home/start', {
    title: 'Tus Plantitas'
  });
};

exports.community = (req, res) => {
  res.render('home/community', {
    title: 'Comunidad'
  });
};

exports.market = (req, res) => {
  res.render('home/market', {
    title: 'Mercado'
  });
};

exports.plantar = (req, res) => {
  res.render('home/plantar', {
    title: 'plantar'
  });
};

const mongoose = require('mongoose');

const travelSchema = new mongoose.Schema({
    initDate: Date,
    endDate: Date,
    originPoint: String,
    endPoint: String,
});

const Travel = mongoose.model('Travel', travelSchema);

module.exports = Travel;

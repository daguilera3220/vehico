const mongoose = require('mongoose');
var Schema = mongoose.Schema

const assetSchema = new Schema({
    name: String,
    brand: String,
    mdl: String,
    type: Number, //Terrestre (0), Maritimo(1), Aereo(3)
    year: Number,
    initPrice: Number,
    initDate: Date,
    capacity: Number,
    onSale: Boolean,
    abi: String,
    addressETH: String,
    owners: [{
      owner: {
        type: Schema.ObjectId, ref: 'User'
      },
      addressETH: String,
      qty: Number,
      selling: Number
    }]
});

const Asset = mongoose.model('Asset', assetSchema);

module.exports = Asset;
